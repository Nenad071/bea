<style>
    @-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}
.blink{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 1.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}
</style>
<!--page_container-->
<div class="page_container">
        <!--slider-->
        <?php 
        include "slider.php";
        ?>       
        <!--//slider-->
                       
        <!--planning-->
        <div class="wrap planning">
            <div class="container">
                
            </div>
        </div>
        <!--//planning-->
        
        <!--Welcome-->
        <div class="wrap block">
            <div class="container welcome_block">
            	<div class="welcome_line welcome_t"></div>
            	<p>
STR  Bea se više od 20 godina bavi uvozom i prodajom perika, umetaka i tupea i trenutno u svom asortimanu ima više od 100 modela ženskih, muških i dečijih perika.</p><br>
<p>Posebna pažnja posvećuje se medicinskom programu tj. perikama koje su posebno dizajnirane i namenjene osobama koje imaju problema sa gubitkom kose. </p>
<hr><br>
<p style="text-transform: none">Perike se izrađuju od engleskog mikrofibera, a dizajniraju u Italiji. Apsolutno su verodostojne, diskretne i pouzdane. Odgovaraju svakom obimu glave, imaju svoje regulatore veličine i jednostavno se postavljaju. Veoma lako se održavaju. 
</p><br> 
<a href="modeli/1"><button class="btn send_btn">Pogledajte našu ponudu &raquo;</button></a>
                
                <div class="welcome_line welcome_b"></div>
            </div>
        </div>
        <!--//Welcome-->         
        
        
       
        
        
        <!--latest posts-->
        <?php
        $modeli_podaci= $db_conn->get_from_db("modeli", "*", "and modeli.istaknuto_naslovna=1");
        
        foreach ($modeli_podaci as $k=>$value){
            
        
        ?>
        
        <div class="wrap block carousel_block">
            <div class="container">
            	<h2 class="upper"><?= $value["model"]?> - Izdvajamo iz ponude</h2>
            	<div class="row">
                    <div class="span12">
                        <ul id="mycarousel2" class="jcarousel-skin-tango">
                            <?php 
                                $kratki_modeli_podaci=$db_conn->get_from_db("proizvodi", "proizvodi.slika,proizvodi.alt_slika,modeli.html_opis,proizvodi.sifra,proizvodi.cena,modeli.cena as 'cena_modela',proizvodi.cena_modela as 'cm'","and modeli.id=".(int)$value["id"]." and proizvodi.prikazi_na_naslovnoj=1 order by proizvodi.sifra", true, "modeli on modeli.id=proizvodi.model_id");
                                //var_dump( $kratki_modeli_podaci);
                                 foreach ($kratki_modeli_podaci as $key=>$v){
                                     $cena_za_prikaz=($v["cm"]==1)?$v["cena_modela"]:$v["cena"];
                                        ?>
                            <li>
                            	<div class="post_carousel">
                                	<img src="<?php echo $v["slika"]; ?>" alt="<?php echo $v["alt_slika"]; ?>" />
                                	<div class="title_t"><a href="<?php echo $v["slika"]; ?>" rel="prettyPhoto[<?php echo $v["html_opis"]; ?>]">Uvećaj sliku</a> <i class="fa fa-search" aria-hidden="true"></i>  </div> <br>
                                         <span style="font-size:16px"><b>Šifra proizvoda:</b> <?php echo $v["sifra"]; ?> </span><br>
                                        <span style="font-size:16px"><b>Cena:</b> <?php echo $cena_za_prikaz; ?></span> 
                                    
                                </div>	
                            </li>
                            
                            <?php
                                 }
                                 ?>
                        </ul>                        
                    </div>                
                </div>                
            </div>
        </div>    
      <?php }  ?>
        
        
        <!--//latest posts--> 
         
        <div class="wrap block">
            <div class="container welcome_block">
            	<div class="welcome_line welcome_t"></div>
                <span style="font-size:30px; color:red;" class="blink">Uz svaku kupljenu periku, poklon MARAMA (turban)</span><br><hr>
            	<span><b>Robu šaljemo pouzećem!</b></span><br>
                Dostava na kućnu adresu ili u bolnicu, u zemlji i inostranstvu. <br><br>
                Kontaktirajte nas:<br><br>

                <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:25px'></i><span style='color:red;font-size:25px;display:inline;font-weight: bold'>+381 62 455-854;</span> <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:25px'></i><span style='color:red;font-size:25px;display:inline;font-weight: bold'>+381 11 2655-854;</span> <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:25px'></i><span style='color:red;font-size:25px;display:inline;font-weight: bold'>+381 11 2693-906;</span> <br><br>
                            
                Radno vreme:<br>
                Radnim danima i subotom i nedeljom od 10:00h do 20:00h 
                
                <div class="welcome_line welcome_b"></div>
            </div>
        </div>
    </div>
<!--//page_container-->
 