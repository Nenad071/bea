
        
    <!--page_container-->
    <div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
                <div class="container">
                    MODELI
                </div>
            </div> 
        </div>
    	<!--MAIN CONTENT AREA-->
        <div class="wrap">
            <div class="container inner_content">
                <div id="options">                                           
                    <ul id="filters">
                         <!--data-option-value=".<?= $v["html_opis"]?>"--> 
                        <?php $kategorije= get_from_db("modeli", "*");
                           foreach ($kategorije as $key=>$v){
                               
                           ?>
                        <li><a href="modeli/<?=$v['id']?>" id="<?= $v["html_opis"]?>" class="btn dark_btn <?=($_GET['id_modela']==$v["id"])?"selected":""?>"><?= $v["model"]?></a></li>                                            
                           <?php 
                           if($_GET['id_modela']==$v["id"])
                           {
                               ?>
                        
                        <script>
                       // window.onload=function(){$("#<?=$v["html_opis"]?>").click()};
                        </script>
                            <?php
                           }
                           
                           }?>
                       
                        
                    </ul>                                           
                    <div class="clear"></div>
                </div>
                <div class="row">
                    <!-- portfolio_block -->
                    <div class="">   
                         <!--kratke -->
                         <?php 
                         $modeli_podaci= get_from_db("proizvodi", "proizvodi.slika,proizvodi.alt_slika,modeli.html_opis,proizvodi.sifra,proizvodi.cena,modeli.cena as 'cena_modela',proizvodi.cena_modela as 'cm'","and proizvodi.model_id=".(int)$_GET['id_modela']." order by proizvodi.sifra", true, "modeli on modeli.id=proizvodi.model_id");
                         
                         foreach ($modeli_podaci as $key=>$v){
                             $cena_za_prikaz=($v["cm"]==1)?$v["cena_modela"]:$v["cena"];
                            ?>
                        <div class="span3 element <?php echo $v["html_opis"]; ?>" data-category="<?php echo $v["html_opis"]; ?>">
                            
                            <div class="hover_img">
                                <img src="<?php echo $v["slika"]; ?>" alt="<?php echo $v["alt_slika"]; ?>" />
                                <span class="portfolio_zoom"><a href="<?php echo $v["slika"]; ?>" rel="prettyPhoto[<?php echo $v["html_opis"]; ?>]"></a></span>
                               
                            </div> <br>
                            <div class="item_description">
                               
                                <div class="descr">
                                    <span style="font-size:16px"><b>Šifra proizvoda:</b> <?php echo $v["sifra"]; ?> </span><br>
                                    <span style="font-size:16px"><b>Cena:</b> <?php echo $cena_za_prikaz; ?></span>
                                </div>
                            </div>                                    
                        </div>
                     <?php  }
                         ?>
                 
                        <div class="clear"></div>
                    </div>   
                    <!-- //portfolio_block -->   
                </div>
            </div>
        </div>
    <!--//MAIN CONTENT AREA-->
    	
    </div>
    <!--//page_container-->
   