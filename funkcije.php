<?php

function get_from_db($table, $fields = "*", $filter = "",$join=false,$join_uslov="") {
    global $db_conn;
    $nizIzDb=array();
    if($join)
    {
        $join_uslov="inner join {$join_uslov}";
    }
    else
    {
        $join_uslov="";
    }
    $upit = "select {$fields} from {$table} {$join_uslov} where 1 {$filter}";
 //echo $upit;

    $query = mysqli_query($db_conn, $upit);
    while($res = mysqli_fetch_array($query))
    {
        $nizIzDb[]=$res;

        
    }
    return $nizIzDb;
}

function insert_update_db($table, $data, $id=0){
     global $db_conn;
    /* var_dump($id);
     die;*/
    if ($id=='0'){
        $string_keys="(";
        $string_values="(";
        foreach ($data as $k=>$v){
            $string_keys.=$k.",";
            $string_values.="'".$v."'".",";
        }
        $string_keys=rtrim($string_keys, ',');
        $string_keys.=")";
        $string_values=rtrim($string_values, ',');
        $string_values.=")";
        $upit="insert into {$table} {$string_keys} values {$string_values}";
          /* echo $upit;
       die();*/
    }else {
         $string_upd="";
   // $string_values="";
        foreach ($data as $k=>$v){
            $string_upd.=$k."="."'".$v."'".",";
           // $string_values.="'".$v."'".",";
        }
        $string_upd=rtrim($string_upd, ',');
        $upit="update {$table} set {$string_upd} where id='{$id}'";
    /* echo $upit;
       die();*/
    }
     $query=mysqli_query($db_conn,$upit);
}

function delete_from_db ($table, $id){
    global $db_conn;
    
    
    $upit="delete from {$table} where id='{$id}'";
    $query=mysqli_query($db_conn,$upit);
}
