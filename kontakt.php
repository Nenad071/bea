<!--
<div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
            	<div class="container">
                   KONTAKT
                </div>
            </div>
        </div>
    	<div class="wrap">
        	<div class="container">
                <section>
                	<div class="row">
                    	<div class="span4" style="width: 60%">
                        	<h2 class="title"><span>Kontakt informacije:</span></h2>
                            <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5662.633258359052!2d20.441828675321734!3d44.79473548172191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a6fe2b8155555%3A0x19160bed8539707d!2sTr%C5%BEni+Centar+SAJAM+%7C+Beogradski+Sajam!5e0!3m2!1ssr!2srs!4v1511543856971" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                            <p>Tržni centar Sajam (Beogradski sajam - Bazar)</p>
                            <p>Hala 11, lokal 218</p>
                            <p>Bulevar Vojvode Mišića 14, 11000 Beograd</p><br>
                            <i class="fa fa-phone" aria-hidden="true"></i>+381 62 455-854; <i class="fa fa-phone" aria-hidden="true"></i>+381 11 2655-854; <i class="fa fa-phone" aria-hidden="true"></i>+381 11 2693-906 <br><br>
                            <p>E-mail: bea@perike.co.rs</p>                           
                        </div>
                        <div class="span8">
                        	<h2 class="title"><span>Kako doći do nas:</span></h2>
                      
                            <div class="contact_form">  
                                <img src='slike/mapasajma.jpg' alt='sajam'>
                            	
                            </div>                   
                        </div>  
                    	       
                               
                	</div>
                </section>
            </div>
        </div>
    </div>-->

<div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
            	<div class="container">
                    KONTAKT
                </div>
            </div>
        </div>
    	<div class="wrap">
        	<div class="container">
                <section>
                	<div class="row">
                    	<div class="span8">
                        	<h2 class="title"><span>Kontakt informacije:</span></h2>
                              <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5662.633258359052!2d20.441828675321734!3d44.79473548172191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a6fe2b8155555%3A0x19160bed8539707d!2sTr%C5%BEni+Centar+SAJAM+%7C+Beogradski+Sajam!5e0!3m2!1ssr!2srs!4v1511543856971" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                           <p>Tržni centar Sajam (Beogradski sajam - Bazar)</p>
                            <p>Hala 11, lokal 218</p>
                            <p>Bulevar Vojvode Mišića 14, 11000 Beograd</p><br>
                            <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:20px'></i><span style='color:red;font-size:20px;display:inline;font-weight: bold'>+381 62 455-854;</span> <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:20px'></i><span style='color:red;font-size:20px;display:inline;font-weight: bold'>+381 11 2655-854;</span> <i class="fa fa-phone" aria-hidden="true" style='color:red;font-size:20px'></i><span style='color:red;font-size:20px;display:inline;font-weight: bold'>+381 11 2693-906;</span> <br><br>
                            <p>E-mail: bea@perike.co.rs</p>                           
                        </div>
                    	<div class="span4">
                        	<h2 class="title"><span>Kako do nas:</span></h2>
                            <div class="contact_form">  
                            	<div id="note"></div>
                                <div id="fields">
                                    <img src='slike/mapasajma.jpg' alt='sajam'>
                                </div>
                            </div>                   
                        </div>                	
                	</div>
                </section>
            </div>
        </div>
    </div>