<!DOCTYPE html>
<html lang="en">
<head>
    <base href="http://localhost/perike/perike/perike_bea/">
<!--    http://www.perike.co.rs/novisajt/-->
<meta charset="utf-8">
<title><?=$bea_seo_titles[$_GET['strana']]?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo ($_GET['strana']=="modeli")?$bea_seo_desc['modeli']:$bea_seo_desc['home']; ?>">
<meta name="author" content="">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" id="camera-css"  href="css/camera.css" type="text/css" media="all">

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/skins/tango/skin.css" />
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->    
</head>
<body>
	<!--header-->
    <div class="header">
    	<div class="wrap">
        	<div class="navbar navbar_ clearfix">
            	<div class="container">
                    <div class="row">
                        <div class="span4">
                        	<div class="logo"><a href="home"><img src="img/logo.png" alt="" /></a></div>                        
                        </div>
                        <div class="span8">
                        	<div class="follow_us">
                                <ul>
                                          <li><a href="https://www.facebook.com/bea.perike" target="_blank" class="social-icon" title="facebook"> <i class="fa fa-facebook" style="font-size:22px;"></i></a></li>
                                          <li><a href="https://www.instagram.com/perike_bea/" target="_blank" class="social-icon" title="instagram"> <i class="fa fa-instagram" style="font-size:22px;"></i></a></li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <?php
                            include "navigation.php";
                            ?>                      
                        </div>
                    </div>                
                </div>
             </div>
        </div>    
    </div>
     <!--//header--> 