<div class="page_container" style='min-height:700px;'>
    	<div class="breadcrumb">
        	<div class="wrap">
                <div class="container">
                    GALERIJA
                </div>
            </div> 
        </div>
    	<!--MAIN CONTENT AREA-->
        <div class="wrap">
            <div class="container inner_content">
                
                <div class="row">
                    <!-- portfolio_block -->
                    <div class="">   
                         <!--kratke -->
                         <?php 
                         $galerija= get_from_db("galerija", "*");
                        foreach ($galerija as $key=>$v){
        ?>
                         
                  <div class="span4 element category01" data-category="category01">
                      
                            <div class="hover_img">
                                <img src="<?= $v['slika'] ?>" alt="" />
                                <span class="portfolio_zoom"><a href="<?= $v['slika'] ?>" rel="prettyPhoto[portfolio1]"></a></span>
                                
                            </div> 
                                                              
                        </div>
                        
                        <?php } ?>
                        
                        <div class="clear"></div>
                        
                    </div>   
                    <!-- //portfolio_block -->   
                </div>
            </div>
        </div>
    <!--//MAIN CONTENT AREA-->
    	
    </div>
