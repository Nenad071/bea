   <!--footer-->
    <div id="footer">
    	
        
        <div class="footer_bottom">
            <div class="wrap">
            	<div class="container">
                	<div class="row">
                		                     
                        </div>
                        <div class="span11">
                        	<div class="foot_center_block">
                            	
                              
                                <div class="clear"></div>
                         
                            	<div class="clear"></div>
                                 
                            	<div class="foot_menu">
                                     
                            		<ul>
                                        <li><a href="home" <?php echo ($_GET['strana']=="home")?"class='current'":"";?>>Početna</a></li>
                                        <li><a href="modeli/1" <?php echo ($_GET['strana']=="modeli")?"class='current'":"";?>>Modeli</a></li>
                                        <li><a href="cenovnik" <?php echo ($_GET['strana']=="cenovnik")?"class='current'":"";?>>Cenovnik</a></li>
                                        <li><a href="galerija" <?php echo ($_GET['strana']=="galerija")?"class='current'":"";?>>Galerija</a></li>
                                        <li><a href="kontakt" <?php echo ($_GET['strana']=="kontakt")?"class='current'":"";?>>Kontakt</a></li>
                                        
                                    </ul>
                                    <div class="copyright" style='margin-top:10px;'>&copy; 2017. Perike Bea - Sva prava zadržana</div>  
                            	</div>
                            </div>                            
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
    <!--//footer-->    

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
       <script src="js/google-code-prettify/prettify.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
    <script type="text/javascript" src="js/camera.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/superfish.js"></script>
   <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script src="js/jquery.isotope.min.js" type="text/javascript"></script>

	<script type="text/javascript" src="js/sorting.js"></script>
    <script type="text/javascript" src="js/jquery.preloader.js"></script>

    <script type="text/javascript" src="js/jquery.jcarousel.js"></script>
    <script type="text/javascript" src="js/jquery.tweet.js"></script>
    <script type="text/javascript" src="js/myscript.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){	
			//Slider
			$('#camera_wrap_1').camera();
			
			//Featured works & latest posts
			$('#mycarousel, #mycarousel2, #newscarousel').jcarousel();													
		});		
			$(document).ready(function(){	
			//prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto();
			
			//Image hover
			$(".hover_img").live('mouseover',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:0.6},500);
					$(".preloader").css({'background':'none'});
				}
			);
			$(".hover_img").live('mouseout',function(){
					var info=$(this).find("img");
					info.stop().animate({opacity:1},500);
					$(".preloader").css({'background':'none'});
				}
			);	
			// Preloader
			$(".projects .element").preloader();	
							
		});
	</script>
    <script src="js/application.js"></script>

</body>
</html>