<?php
if (!isset($_SESSION['adminSes']))
{
    include "404.php";
    die();
}

   $naziv_kategorije="";
    $istaknuto_naslovna="";
    $checked=0;
    $hidden_id_kat="";
     $dugmad='<input type="submit" name="unesi_btn" value="unesi">';
     $orient_port="checked";
        $orient_land="";
        $cena_kategorije="";
$niz_gresaka="";
if (isset($_POST["izaberi_btn"])){
    $id_kategorija=(int)$_POST["modeli"];
    if($id_kategorija>0)
    {
    $podaci_kategorije= get_from_db("modeli", "*", "and modeli.id={$id_kategorija}");
   // var_dump($podaci_kategorije);
    $naziv_kategorije=$podaci_kategorije[0]["model"];
    $cena_kategorije=$podaci_kategorije[0]["cena"];
    $hidden_id_kat="<input type='hidden' name='id_kat' value='".$id_kategorija."'>";
    $dugmad='<input type="submit" name="izmeni_btn" value="izmeni">
    <input type="submit" name="obrisi_btn" value="obrisi">';
    if ($podaci_kategorije[0]["istaknuto_naslovna"]==1){
        $istaknuto_naslovna="checked";
    }
    else {
        $istaknuto_naslovna="";
    } 
    if ($podaci_kategorije[0]["orientation"]==0){
        $orient_port="checked";
        $orient_land="";
    }
    else {
        $orient_port="";
        $orient_land="checked";
    } 
    $checked=$podaci_kategorije[0]["istaknuto_naslovna"];
    }
}

 

if (isset($_POST["unesi_btn"])){
      unset($_POST["unesi_btn"]);
    if(!isset ($_POST["istaknuto_naslovna"])){
        $_POST["istaknuto_naslovna"]=0;
        
    }else{
        $_POST["istaknuto_naslovna"]=1;
        
    }
  
     $validator = new \Classes\Validator($_val_added_rules_kat);

        $data = $_POST;

        if (!$validator->isValid($data,$_val_rules_kat)) {
            
           //var_dump($validator->getErrors()->errors);
            $errors=$validator->getErrors();
            foreach($validator->getErrors()->errors as $err)
            {
              // var_dump($err->messages);
               $niz_gresaka.="<span style='color:red'>".$err->messages[0]."</span>"."<br>";
            }
            $niz_gresaka.="<br><br>";
                   if ($_POST["orientation"]==0){
                $orient_port="checked";
                $orient_land="";
            }
            else {
                $orient_port="";
                $orient_land="checked";
            }
            if($_POST["istaknuto_naslovna"]==0)
            {
                $istaknuto_naslovna="";
            }
            else 
            {
                $istaknuto_naslovna="checked";
            }
            $naziv_kategorije=$_POST["model"];
            $cena_kategorije=$_POST["cena"];
            //die;
        } 
        else
        {
               $_POST["html_opis"]="model_".uniqid();
    
               insert_update_db("modeli", $_POST);
        }
}
if (isset($_POST["izmeni_btn"])){
    if(!isset($_POST['id_kat'])){
        $id_kat=null;
    }
    else{
    $id_kat=(int)$_POST['id_kat'];
    }
    /*var_dump($id_kat);
    die;*/
    unset($_POST["izmeni_btn"]);
    unset($_POST["id_kat"]);
   if(!isset ($_POST["istaknuto_naslovna"])){
        $_POST["istaknuto_naslovna"]=0;
        
    }else{
        $_POST["istaknuto_naslovna"]=1;
        
    }

    $validator = new \Classes\Validator($_val_added_rules_kat);

        $data = $_POST;
    if (!$validator->isValid($data,$_val_rules_kat)) {
            
           //var_dump($validator->getErrors()->errors);
            $errors=$validator->getErrors();
            foreach($validator->getErrors()->errors as $err)
            {
              // var_dump($err->messages);
               $niz_gresaka.="<span style='color:red'>".$err->messages[0]."</span>"."<br>";
            }
            $niz_gresaka.="<br><br>";
                if ($_POST["orientation"]==0){
                $orient_port="checked";
                $orient_land="";
            }
            else {
                $orient_port="";
                $orient_land="checked";
            }
            if($_POST["istaknuto_naslovna"]==0)
            {
                $istaknuto_naslovna="";
            }
            else 
            {
                $istaknuto_naslovna="checked";
            }
            $naziv_kategorije=$_POST["model"];
            $cena_kategorije=$_POST["cena"];
            $hidden_id_kat="<input type='hidden' name='id_kat' value='".$id_kat."'>";
    $dugmad='<input type="submit" name="izmeni_btn" value="izmeni">
    <input type="submit" name="obrisi_btn" value="obrisi">';
            //die;
        } 
        else {
    insert_update_db("modeli", $_POST,$id_kat);
        }
}
if (isset($_POST["obrisi_btn"])){
     $id_kat=$_POST['id_kat'];
    delete_from_db("modeli",(int)$id_kat);

}

?>

<div class="page_container">
    	<div class="breadcrumb">
        	<div class="wrap">
            	<div class="container">
                    ADMIN / KATEGORIJE
                </div>
            </div>
        </div>
    	<div class="wrap">
        	<div class="container">

<form method='post' action='' name='izaberi_modeli'>
    <select name='modeli'>
        <option value='0'>--Odaberite model--</option>
        <?php
        $kategorije= get_from_db("modeli", "*");
        foreach ($kategorije as $key=>$v){
        ?>
        <option value='<?= $v["id"]?>'>
            <?= $v['model']?>
        </option>
           <?php }     ?>
       
    </select> <br><br>
   
    <input type="submit" value='Izaberi' name="izaberi_btn">
    
</form>
                    <br>
 <?=$niz_gresaka?>
<form method='post' action='' name='unesi_izmeni'>
    Naziv Modela <br>
    <input type='text' name='model' value="<?=$naziv_kategorije?>">
    <br>
     Cena Modela <br>
    <input type='text' name='cena' value="<?=$cena_kategorije?>">
    <br>
    Ovaj model je istaknut na naslovnoj
    <input type='checkbox' name='istaknuto_naslovna' value='<?=$checked?>' <?= $istaknuto_naslovna?>><br><br>
    Landscape:
    <input type='radio' name='orientation' value='0' <?=$orient_port?>>
    Portrait:
    <input type='radio' name='orientation' value='1' <?=$orient_land?>><br>
    <br><br>
    <?=$hidden_id_kat?>
     <?=$dugmad?>
   
   
    
    
</form>

            </div>
        </div>
    </div>